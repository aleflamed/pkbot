from os import environ
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, TIMESTAMP, ForeignKey, PrimaryKeyConstraint, Boolean
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

base = declarative_base()

Session = sessionmaker()


class FluidType(base):
    __tablename__ = 'fluidtype'
    type = Column(Integer, nullable=False, primary_key=True)
    description = Column(String)


class Fluid(base):
    __tablename__ = 'fluid'
    ts = Column(TIMESTAMP, primary_key=True)
    type = Column(Integer, ForeignKey('fluidtype.type'))
    description = Column(String)
    estimated_ts = Column(Boolean, default=False)
    maybe = Column(Boolean, default=False)


def make_session_maker():
    engine = create_engine(environ['PKBOT_DATABASE_URL'], echo=True)
    # Tables already created.
    #base.metadata.create_all(engine)
    Session.configure(bind=engine)
    return Session
