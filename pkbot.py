#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Simple Bot to reply to Telegram messages.

This program is dedicated to the public domain under the CC0 license.

This Bot uses the Updater class to handle the bot.

First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

from pdb import set_trace as b
import re
from os import environ
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging
from datetime import timedelta, datetime

from db import make_session_maker, Fluid, FluidType

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


Session = make_session_maker()


def init_types():
    session = Session()
    ret = {x.description: x.type for x in session.query(FluidType).all()}
    session.close()
    return ret

fluidtypes = init_types()
fluidtype_to_text = {v: k for k, v in fluidtypes.items()}

# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    """Send a message when the command /start is issued."""
    update.message.reply_text('This is Panda Kraken Bot. Hi!')


def help(bot, update):
    """Send a message when the command /help is issued."""
    update.message.reply_text('Keeps track of pipi or kaki messages. Any message matching will be logged in a table. To see a summary of the table write message "summary"')


class TestException(Exception):
    pass


class Event:
    def __init__(self, type, time):
        self.type = type if type == TEST_ALIAS else fluidtypes[type]
        self.time = time

    def __str__(self):
        return f'Event {self.type} {self.time}'


aliases = {
    'pipi': 'pipi',
    'פיפי': 'pipi',
    'kaki': 'kaki',
    'קקי': 'kaki',
    'fire': 'fire'
}

TEST_ALIAS = 'fire'

assert set(aliases.values()) <= set(fluidtypes.keys()) | {TEST_ALIAS}


def parse_events(msg):
    time = msg.date
    text = msg.text.lower()
    m = re.search('(?P<hour>\d+):(?P<minute>\d+)', text)
    if m:
        g = m.groupdict()
        hour = int(g['hour'])
        minute = int(g['minute'])
        time = time.replace(hour=hour, minute=minute, second=0, microsecond=0)
    # TODO - handle both pipi and kaki in same message
    contains = {aliases.get(w, None) for w in text.split()} - {None}
    ret = []
    for i, type in enumerate(sorted(contains)): # TODO: pipi before kaki :)
        ret.append(Event(type=type, time=time + timedelta(seconds=i)))
    return ret


def handle_event(event):
    if event.type == TEST_ALIAS:
        raise TestException()
    session = Session()
    try:
        session.add(Fluid(ts=event.time, type=event.type))
        session.commit()
    finally:
        session.close()


def as_min_width_table(rows):
    rows = [[str(x) for x in row] for row in rows]
    row_widths = [max([len(rows[i][j]) for i in range(len(rows))]) + 1 for j in range(len(rows[0]))]
    fmts = ['{:%d}' % rw for rw in row_widths]
    ret = '\n'.join([''.join(fmt.format(r) for fmt, r in zip(fmts, row)).strip() for row in rows])
    return ret


def get_today_events(type):
    session = Session()
    now = datetime.now()
    today = f"'{now.year}-{now.month}-{now.day}'"
    try:
        return list(session.execute(f'select count(*) from fluid where type = {type} and ts > {today}'))[0][0]
    finally:
        session.close()


def summary_markdown():
    session = Session()
    results = [('error', 'could not retrieve summary')]
    try:
        results = list(session.execute('table fluid_daily'))
    finally:
        session.close()
    rows = ['date pipi kaki'.split()] + results
    table = as_min_width_table(rows)
    return f'```\n{table}\n```'


def transpose_table(rows):
    return [[rows[i][j] for i in range(len(rows))] for j in range(len(rows[0]))]


def do_sql(update, text, transpose=False):
    session = Session()
    error = None
    try:
        ret = list(session.execute(text))
    except Exception as e:
        error = str(e)
    finally:
        session.commit()
        session.close()
    if transpose and error is None:
        ret = transpose_table(ret)
    reply = f'```\n{as_min_width_table(ret).strip()}\n```' if error is None else error
    update.message.reply_markdown(reply)


def parse_message(bot, update):
    """Look for special strings and track them."""
    #update.message.reply_text(update.message.text)
    text = update.message.text.lower().strip()
    print(f"got message: {text}")
    if text == 'summary':
        return update.message.reply_markdown(summary_markdown())
    if text.startswith('sql'):
        # here goes the neighbourhood
        return do_sql(update, update.message.text.strip()[4:])
    if text.startswith('tsql'):
        return do_sql(update, update.message.text.strip()[4:], transpose=True)
    try:
        eventiter = parse_events(update.message)
    except Exception as e:
        return update.message.reply_text(f'oops, the little bot is broken, your message was ignored, please try again! message: {update.message}\n{e!r}')
    for event in eventiter:
        try:
            handle_event(event)
        except TestException as e:
            update.message.reply_text(f'got the test event. Today had {get_today_events(1)} and {get_today_events(2)}')
        except Exception as e:
            update.message.reply_text(f'Error logging {event}: {e}')
        else:
            count = get_today_events(event.type)
            update.message.reply_text(f'thanks, logged {event.type}, today had {count} {fluidtype_to_text[event.type]}')


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    """Start the bot."""
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(environ['TELEGRAM_PKBOT_API_TOKEN'])

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, parse_message))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


def test_db():
    session = Session()
    b()


if __name__ == '__main__':
    #test_db()
    main()
